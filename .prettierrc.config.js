module.exports = {
  printWidth: 78,
  tabWidth: 4,
  useTabs: false,
  semi: true,
  singleQuote: true,
  trailingComma: "all"
  /*
  bracketSpacing: true,
  jsxBracketSameLine: false,
  arrowParens: 'avoid',
  rangeStart: 0,
  rangeEnd: Infinity,
  requirePragma: true,
  insertPragma: true,
  proseWrap: 'preserve'
  */
}
