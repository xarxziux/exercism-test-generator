const fs = require('fs');
const specToTest = require('./spec_to_test');
const settings = require('../data/settings.json');
const exercises = require('../data/exercises.json');

// console.log(fs.existsSync(settings.source));
// console.log(fs.existsSync(settings.target));
// console.log(exercises);
// console.log(fs.existsSync(specFile));
// console.log(fs.existsSync(testFile));

exercises.forEach(name => {
    const specFile = settings.source + name + '/canonical-data.json';
    const specData = require(specFile);
    const testFile =
        settings.target + name + '/test_' + name + '.R';
    const testData = specToTest({specData, name});
    console.log(`Creating ${testFile}...`);
    fs.writeFileSync(testFile, testData);
});
