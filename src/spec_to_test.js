const typeOf = require('number-detect').numberDetect;

const getSeqArray = x => Array(x).fill().map((_, i) => i + 1);
const addUpper = str => `${str[0].toUpperCase()}${str.slice(1)}`;

const getHead = name => `source("./${name}.R")
library(testthat)

context("${name}")
`;

const getTail = name => `
message("All tests passed for exercise: ${name}")

`;

const paramFns = {
    number: x => x.toString(),
    string: x => '"' + x + '"',
    boolean: x => x ? 'TRUE' : 'FALSE',
    Array: x => 'c(' +
        x.map(y => paramFns[typeOf(y)](y)).join(',') +
        ')'
};

const paramToString = param => (
    Object.keys(param)
        .map(x => paramFns[typeOf(param[x])](param[x]))
        .join(',')
);

const getFirstTest = (specData, name) => `
test_that("${addUpper(specData.cases[0].description)}", {
  input <- "${paramToString(specData.cases[0].cases[0].input)}"
  expect_equal(${name}(input), "${specData.cases[0].cases[0].expected}")
})
`;

const getNextTest = (specData, name) => i => `
test_that("${addUpper(specData.cases[0].cases[i].description)}", {
  input <- "${specData.cases[0].cases[i].input.phrase}"
  expect_equal(${name}(input), "${specData.cases[0].cases[i].expected}")
})
`;

module.exports = ({specData, name}) => {
    const head = getHead(name);
    const tail = getTail(name);
    const firstCase = getFirstTest(specData, name);
    const testCases = getSeqArray(specData.cases[0].cases.length - 1)
        .map(getNextTest(specData, name))
        .join('');

    return head + firstCase + testCases + tail;
};
