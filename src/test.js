const spec = require('../exercises/acronym/canonical-data.json');
const name = 'acronym';

export const specToTest = (  {   spec, name    }    ) => {
const head = `
source("./${name}.R")
library(testthat)

context("${name}")

`;

const tail = `
message("All tests passed for exercise: ${name}")

`;

const nextTest = (_, x) => `
test_that("${spec.cases[0].cases[x].description}", {
  input <- "${spec.cases[0].cases[x].input.phrase}"
  expect_equal(${name}(input), "${spec.cases[0].cases[x].expected}")
})

`;

const testCases = Array(spec.cases[0].cases.length)
  .fill(null)
  .map(nextTest);

return data = head + testCases.join('') + tail;
}
//console.log (data);
